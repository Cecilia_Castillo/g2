---
title: "g2"
author: "Cecilia Castillo"
date: "28-06-2021"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

#Agua Potable

##Carga del dataset
```{r}
dataset <- "water_potability.csv"
data <- read.csv(dataset, sep=",")
```

###Dimensión del dataset
```{r}
dim(data)

```

###Nombres de las columnas
```{r}
colnames(data)
```

###Estructura del dataset
```{r}
str(data)
```

###Resumen estadístico básico
```{r}
summary(data)
```

##Identificación de variables cualitativas y cuantitativas
```{r}
data$city <- as.factor(data$city)
data$area <- as.factor(data$area)
data$type.emergency <- as.factor(data$type.emergency)
data$Potability <- as.factor(data$Potability)
summary(data)
```

##Separar el dataset el grupos
```{r}
otras <- subset(data, city == "Santiago" | city == "Rancagua" | city == "Chillan" | city == "Temuco")

Maule <- subset(data, city == "Talca" | city == "CuricÃ³" | city == "Linares")

#resumen
summary(otras)
summary(Maule)
```

```{r}
//as.numeric no me funciona en windows

```


###Diagramas
##Para variables cuantitativas
```{r}
plot(x=otras$time.emergency)
plot(x=otras$ph)
plot(x=otras$Hardness)
plot(x=otras$Solids)
plot(x=otras$Chloramines)
plot(x=otras$Sulfate)
plot(x=otras$Conductivity)
plot(x=otras$Organic_carbon)
plot(x=otras$Trihalomethanes)
plot(x=otras$Turbidity)
```

##Para variables cualitativas
```{r}
plot(x=otras$city, main="Ciudades no ubicadas en Maule", xlab="Ciudades", ylab="Frecuencia")
plot(x=Maule$city, main="Ciudades del Maule", xlab="ciudades", ylab="Frecuencia")

plot(x=otras$area, main="Tipos de àreas en ciudades no ubicadas en Maule", xlab="Àreas Rural/Urbano", ylab="Frecuencia")
plot(x=Maule$area, main="Tipos de àreas en ciudades ubicadas en Maule", xlab="Àreas Rural/Urbano", ylab="Frecuencia")

plot(x=otras$type.emergency, main="Tipos de emergencia en ciudades no ubicadas en Maule", xlab="Tipo emergencia de 1 a 8", ylab="Frecuencia")
plot(x=Maule$type.emergency, main="Tipos de emergencia en ciudades ubicadas en Maule", xlab="Tipo emergencia de 1 a 8", ylab="Frecuencia")

plot(x=otras$Potability, main="Potabilidad en ciudades no ubicadas en Maule", xlab="Potabilidad 0 o 1", ylab="Frecuencia")
"El 0 corresponde a no potable, por lo tanto la mayorìa del agua en ciudades no ubicadas en Maule son no potables"
plot(x=otras$Potability, main="Potabilidad en ciudades ubicadas en Maule", xlab="Potabilidad 0 o 1", ylab="Frecuencia")
"Correspondiendo 0 a no potable, la mayorìa de agua en la regiòn del Maule es no potable"

#summary(plot(otras))
#summary(plot(Maule))
```


## Detección de Outliers/ valores fuera de rango
```{r}
boxplot(data$ph)
outliers <- boxplot.stats(data$ph)$out
outliers
#"No los quité porque son demasiados outliers"
```

```{r}
boxplot(data$Hardness)
outliers <- boxplot.stats(data$Hardness)$out
outliers
```


```{r}
if(!is.null(outliers)){
  data$Hardness <- ifelse(data$Hardness %in% outliers,
                    NA, data$Hardness)}
boxplot(data$Hardness)
```

```{r}
boxplot(data$Solids)
outliers <- boxplot.stats(data$Solids)$out
outliers
"No se eliminan outliers porque son muchos"
```

```{r}
boxplot(data$Chloramines)
outliers <- boxplot.stats(data$Chloramines)$out
outliers
"No se eliminan outliers porque son muchos"
```


```{r}
boxplot(data$Sulfate)
outliers <- boxplot.stats(data$Sulfate)$out
outliers
```


```{r}
boxplot(data$Conductivity)
outliers <- boxplot.stats(data$Conductivity)$out
outliers
```

```{r}
if(!is.null(outliers)){
  data$Conductivity <- ifelse(data$Conductivity %in% outliers,
                    NA, data$Conductivity)}
boxplot(data$Conductivity)
```

```{r}
boxplot(data$Organic_carbon)
outliers <- boxplot.stats(data$Organic_carbon)$out
outliers
```


```{r}
if(!is.null(outliers)){
  data$Organic_carbon <- ifelse(data$Organic_carbon %in% outliers,
                    NA, data$Organic_carbon)}
boxplot(data$Organic_carbon)
```

```{r}
boxplot(data$Trihalomethanes)
outliers <- boxplot.stats(data$Trihalomethanes)$out
outliers
```


```{r}
if(!is.null(outliers)){
  data$Trihalomethanes <- ifelse(data$Trihalomethanes %in% outliers,
                    NA, data$Trihalomethanes)}
boxplot(data$Trihalomethanes)
```

```{r}
boxplot(data$Turbidity)
outliers <- boxplot.stats(data$Turbidity)$out
outliers
```


```{r}
if(!is.null(outliers)){
  data$Turbidity <- ifelse(data$Turbidity %in% outliers,
                    NA, data$Turbidity)}
boxplot(data$Turbidity)
```

